import React, {Component} from 'react'
import Post from './post'
import axios from 'axios'


export default class TimeLine extends Component{
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            boastsOnly: [],
            roastsOnly: [],
            mostVoted: [],
            mode: 'All'
        }
        this.changeMode = this.changeMode.bind(this)
        this.refreshList = this.refreshList.bind(this)

    }
    componentDidMount(){
        this.refreshList()  
    }

    refreshList(){
        axios.get('http://localhost:8000/api/posts/')
        .then(res => {
            this.setState({
                posts: res.data,
                boastsOnly: res.data.filter(x => x.typeOfPost === 'Boast'),
                roastsOnly: res.data.filter(x => x.typeOfPost === 'Roast'),
                mostVoted: res.data.sort((a, b) => {
                    return a.like < b.like ? 1 : -1
                })

            })
        }) 
    }

    changeMode(mode){
        this.setState({
            mode: mode
        })

        console.log(this.state)
    }
    
    




    render(){
        return(
            <div>
                <div style={{display: 'flex', justifyContent: "space-between"}}>
                <button onClick={() => this.changeMode('Boast')}>Boasts Only</button>
                <button onClick={() => this.changeMode('Roast')}>Roasts Only</button>
                <button onClick={() => this.changeMode('Most')}>Ordered List by most voted</button>
                </div>
            <div>
            {this.state.mode === 'All' ? this.state.posts.map(p => <Post refresh={this.refreshList} post={p}/>) : <div></div>}
            {this.state.mode === 'Boast' ? this.state.boastsOnly.map(p => <Post refresh={this.refreshList} post={p}/>) : <div></div>}
            {this.state.mode === 'Roast' ? this.state.roastsOnly.map(p => <Post refresh={this.refreshList} post={p}/>) : <div></div>}
            {this.state.mode === 'Most' ? this.state.mostVoted.map(p => <Post refresh={this.refreshList} post={p}/>) : <div></div>}
            </div>
            </div>
        )
    }


}