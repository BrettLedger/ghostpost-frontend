import React, {Component} from 'react'
import axios from 'axios'

export default class PostForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            body: '',
            type_of_post: 'Boast',
            upvotes: 0,
            downvotes: 0,
            magic: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.typeChange =this.typeChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        this.setState({body: event.target.value});
        console.log(this.state)
      }

    typeChange(event){
        this.setState({
            type_of_post: event.target.value
        })

        console.log(this.state)
    }

    handleSubmit(event){
        event.preventDefault()

        const data = {
            typeOfPost: this.state.type_of_post,
                body: this.state.body,
                like: this.state.upvotes,
                dislike: this.state.downvotes
        }

         fetch('http://localhost:8000/api/posts/', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
        .then(res => {
           return res.json()
        })
        .then(data => console.log(data))
    }
    render(){
        return(
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                    <input type="text" value={this.state.body} onChange={this.handleChange} />
                    <select onChange={this.typeChange} as='select'>
                        <option>Boast</option>
                        <option>Roast</option>
                    </select>
                </label>
                <input type="submit" value="Submit" />
            </form>
        )
    }
}