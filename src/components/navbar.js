import React, {Component} from 'react'
import { useHistory } from 'react-router-dom';

export default class NavBar extends Component{

    render(){
        return(
            <div>
                <ul style={{display: 'flex', justifyContent: 'space-between'}}>
                    <li><a href="/">Home</a></li>
                    <li><a href="/post">Post</a></li>
                </ul>
            </div>
        )
    }
}