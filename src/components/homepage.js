import React, {Component} from 'react'
import NavBar from "./navbar"
import TimeLine from "./timeline"


export default class HomePage extends Component{
    render(){
        return(          
            <div>
                <NavBar/>
                <TimeLine/>
            </div>
        )
    }
}
