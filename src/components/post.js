import React, {Component} from 'react'
import axios from 'axios'

export default class Post extends Component{
    constructor(props){
        super(props)
        this.state = {
            refresh: ''
        }
    }

    dislike = id => {
        axios.get(`http://localhost:8000/api/posts/${id}/`)
        .then(res => {
            axios.put(`http://localhost:8000/api/posts/${id}/`, {
                dislike: res.data.dislike += 1,
                body: res.data.body
            })

            this.props.refresh() 
        })

        
    }
    like = id => {
        axios.get(`http://localhost:8000/api/posts/${id}/`)
        .then(res => {
            axios.put(`http://localhost:8000/api/posts/${id}/`, {
                like: res.data.like += 1,
                body: res.data.body
            })

            this.props.refresh()
            
            
        })

        
    }

    render(){
        return(
            <div style={{border: "1px solid black", borderRadius: "8px", display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <h1>{this.props.post.typeOfPost}</h1>
                <p>{this.props.post.body}</p>
                <p>{this.props.post.timestamp}</p>
                <p>Likes:{this.props.post.like} </p>
                <p>Dislikes:{this.props.post.dislike} </p>
                <div style={{display: 'flex', justifyContent: 'space-around'}}>
                    <button onClick={() => this.like(this.props.post.id)}>Like</button>
                    <button onClick={() => this.dislike(this.props.post.id)}>Dislike</button>
                </div>
            </div>
        )
    }
}