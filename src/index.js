import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import PostForm from './components/postForm'
import * as serviceWorker from './serviceWorker';
import { Switch, Route, BrowserRouter as Router} from 'react-router-dom';


const routing = (
    <Router>
      <div>
        <br/>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/post" component={PostForm} />
          <Route path="/deletesuccess" component={App} />
        </Switch>
      </div>
    </Router>
    
  )



ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();